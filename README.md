# doc

[git](git.md)

[centos](centos.md)

[docker](docker.md)

[node](node.md)

[vscode](vscode.md)

[ubuntu + gitlab-runner + nginx + node](ubuntu.md)

[ubuntu + gitlab-runner + docker](ubuntu_docker.md)

[mac 系统 uni-app 自动打包](uni-app.md)
