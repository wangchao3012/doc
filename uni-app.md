## mac 系统 uni-app 自动打包需要安装软件

安装 md5 计算命令

brew install md5sha1sum

gitlab-runner 的 config.toml 文件路径
~/.gitlab-runner/

安装 realpath 命令支持

```bash
brew install coreutils
```

注：安装 coreutils 失败可执行下面命令

```bash
brew unlink md5sha1sum
```

### 常见问题

#### 无法调用 cli 命令

注：mac 默认脚本执行类型与 gitlab-runner 脚本类型不同，修改默认脚本类型可解决

![图片示例](doc/uni-app/1.png)
![图片示例](doc/uni-app/2.png)
![图片示例](doc/uni-app/3.png)
