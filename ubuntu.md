ubuntu + gitlab-runner + nginx + node

[win10 linux 子系统](https://www.cnblogs.com/lialong1st/p/12004080.html)

[安装 gitlab-runner](https://docs.gitlab.com/runner/install/linux-repository.html)

[安装 node](https://www.cnblogs.com/feiquan/p/11223487.html)

[安装 nginx](http://nginx.org/en/linux_packages.html#Ubuntu)

[注册 gitlab-ci](https://docs.gitlab.com/runner/register/index.html)

[开放端口](https://blog.csdn.net/ltstud/article/details/78059741)

[gitlab-runner 赋 root 权限](https://www.cnblogs.com/qypt2015/p/6918336.html)

ls -lh --time=atime 查看文件读取时间命令
修改文件读取时间

touch -a -t `date +"%Y%m%d%H%M.%S"` ${pgmd5}.tar.gz

定时任务
sudo crontab -e

重启定时任务服务
sudo service cron restart

0 0 \* \* \* /data/autoTask/cleanFile.sh

<!-- 清理60前访问的文件 -->

find /data/.npm/cache -atime +60 -type f -exec rm -rf {} \;
